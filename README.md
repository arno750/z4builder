# Z4 library builder

## Description

**z4builder** est un outil de construction de la bibliothèque cliente Z4 à partir du modèle.

```mermaid
graph LR

z4model --- z4builder
z4builder -- build --> z4cpp
z4builder -- build --> z4java
z4builder -- build --> z4python
z4builder -- build --> z4rust
```

## Utilisation

**z4builder** est utilisé directement par le plugin Maven **z4builder-maven-plugin**. Il peut également être lancé directement en ligne de commande (CLI). Enfin, il est possible d'utiliser le script de génération `build.sh` qui permet de vérifier le bon fonctionnement de toute la chaîne.

### Utilisation nominale

Le plugin Maven [z4builder-maven-plugin](https://www.gitlab.com/arno750/z4builder-maven-plugin) dépend directement de **z4-builder** et en appelle les méthodes suivantes :

```java
import z4.builder.JavaLibraryBuilder;

new JavaLibraryBuilder().buildSources(modelDirectory, generatedSourcesFolder);
new JavaLibraryBuilder().buildResources(modelDirectory, generatedResourcesFolder);
```

Le paramètre `modelDirectory` correspond au répertoire où se trouve le modèle Z4, dans lequel on attend notamment le fichier `model_parameters.yml`. Une valeur usuelle est `$HOME/target/model`

Le paramètre `generatedSourcesFolder` correspond au répertoire des sources générées de Maven, en général `$HOME/target/generated-sources`.

### Utilisation du CLI

Il est possible de lancer directement l'outil **z4-builder** en spécifiant :

- le répertoire du modèle
- la langage
- le répertoire de destination des sources.

Par exemple, si le répertoire du modèle est `../z4model/target/model` et le répertoire cible est `../z4java/src-gen/` :

```bash
mvn -U clean package

VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)                                                                                           

java -jar target/z4builder-$VERSION-jar-with-dependencies.jar ../z4model/target/model java ../z4java/src-gen/

```

### Utilisation du script `build.sh`

Afin de valider des modifications sur l'outil **z4builder**, il est possible de lancer le script `build.sh`.

Le préalable est d'avoir les différents projets organisés selon l'arborescence suivante :

```txt
  |-- builder
  |      |-- z4builder-maven-plugin
  |      |-- z4finder-maven-plugin
  |      |-- z4builder
  |-- libs
  |      |-- z4java
  |      |-- z4python
  |-- z4model
```

Le script `build.sh` effectue dans l'ordre les tâches suivantes :

- Sur le projet **z4builder**
  - Génération de **z4builder** et installation dans le dépôt Maven local
  - Récupération de la version de **z4builder** indiqué dans le `pom.xml`
- Passage au projet **z4model**
  - Génération des resources
- Passage au projet **z4builder-maven-plugin**
  - Modification du `pom.xml` afin d'utiliser la version locale de **z4builder**
  - Génération de **z4builder-maven-plugin** et installation dans le dépôt Maven local
  - Récupération de la version de **z4builder-maven-plugin** indiqué dans le `pom.xml`
- Passage au projet **z4finder-maven-plugin**
  - Génération de **z4finder-maven-plugin** et installation dans le dépôt Maven local
  - Récupération de la version de **z4finder-maven-plugin** indiqué dans le `pom.xml`
- Passage au projet **z4java** ou **z4python**
  - Modification du `pom.xml` afin d'utiliser la version locale de **z4builder-maven-plugin**
  - Modification du `pom.xml` afin d'utiliser la version locale de **z4finder-maven-plugin**
  - Génération de **z4java** ou de **z4python**
