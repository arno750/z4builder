#! /bin/bash

NORMAL="\\033[0;39m"
RED="\\033[1;31m"
GREEN="\\033[1;32m"
YELLOW="\\033[1;33m"
BLUE="\\033[1;34m"
PURPLE="\\033[1;35m"
CYAN="\\033[1;36m"

BASE=$(readlink -f $0)
BASE=$(dirname $BASE)
BASE=$(dirname $BASE)

checkFolder() {
  if [ ! -d $BASE/$PROJECT ]
  then
    echo -e "${RED}#####################################################################"
    echo -e "${RED}## Folder $BASE/$PROJECT missing!"
    echo -e "${RED}#####################################################################"
    echo -e "${NORMAL}"
    echo
    exit 1
  fi
  cd $BASE/$PROJECT
}

checkCode() {
  if [ $CODE -ne 0 ]
  then
    echo -e "${RED}#####################################################################"
    echo -e "${RED}## Error while generating $PROJECT!"
    echo -e "${RED}#####################################################################"
    echo -e "${NORMAL}"
    echo
    exit 1
  fi
}

echo -e "${GREEN}################################################################"
echo -e "${GREEN}##                                                            ##"
echo -e "${GREEN}##                         z4builder                          ##"
echo -e "${GREEN}##                                                            ##"
echo -e "${GREEN}################################################################"
echo -e "${NORMAL}"

PROJECT=z4-builder
mvn clean install
CODE=$?
checkCode
Z4_BUILDER_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
echo "Z4_BUILDER_VERSION=${Z4_BUILDER_VERSION}"


echo -e "${GREEN}################################################################"
echo -e "${GREEN}##                                                            ##"
echo -e "${GREEN}##                   z4builder-maven-plugin                   ##"
echo -e "${GREEN}##                                                            ##"
echo -e "${GREEN}################################################################"
echo -e "${NORMAL}"


PROJECT=z4builder-maven-plugin
checkFolder
mvn versions:set-property -Dproperty=z4-builder.version -DnewVersion=${Z4_BUILDER_VERSION}
mvn clean install
CODE=$?
checkCode
Z4_BUILDER_MAVEN_PLUGIN_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
mv pom.xml.versionsBackup pom.xml
echo "Z4_BUILDER_MAVEN_PLUGIN_VERSION=${Z4_BUILDER_MAVEN_PLUGIN_VERSION}"

echo -e "${GREEN}################################################################"
echo -e "${GREEN}##                                                            ##"
echo -e "${GREEN}##                   z4finder-maven-plugin                    ##"
echo -e "${GREEN}##                                                            ##"
echo -e "${GREEN}################################################################"
echo -e "${NORMAL}"


PROJECT=z4finder-maven-plugin
checkFolder
mvn clean install
CODE=$?
checkCode
Z4_FINDER_MAVEN_PLUGIN_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
echo "Z4_FINDER_MAVEN_PLUGIN_VERSION=${Z4_FINDER_MAVEN_PLUGIN_VERSION}"

echo -e "${GREEN}################################################################"
echo -e "${GREEN}##                                                            ##"
echo -e "${GREEN}##                           z4-java                          ##"
echo -e "${GREEN}##                                                            ##"
echo -e "${GREEN}################################################################"
echo -e "${NORMAL}"


PROJECT=z4-java
checkFolder
mvn versions:set-property -Dproperty=z4builder.maven.version -DnewVersion=${Z4_BUILDER_MAVEN_PLUGIN_VERSION}
mvn versions:set-property -Dproperty=z4finder.maven.version -DnewVersion=${Z4_FINDER_MAVEN_PLUGIN_VERSION}
mvn clean package
CODE=$?
mv pom.xml.versionsBackup pom.xml

if [ $CODE -eq 0 ]
then
  echo -e "${GREEN}################################################################"
  echo -e "${GREEN}################################################################"
  echo -e "${GREEN}########################### SUCCEED ############################"
  echo -e "${GREEN}################################################################"
  echo -e "${GREEN}################################################################"
  echo -e "${NORMAL}"
else
  echo -e "${RED}################################################################"
  echo -e "${RED}################################################################"
  echo -e "${RED}############################ FAILED ############################"
  echo -e "${RED}################################################################"
  echo -e "${RED}################################################################"
  echo -e "${NORMAL}"
fi

