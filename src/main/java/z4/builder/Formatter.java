package z4.builder;

import org.apache.commons.lang3.StringUtils;

/**
 * This class provides helper functions for naming classes, attributes and
 * parameters.
 * 
 * @author Arnaud Wieland
 *
 */
public class Formatter {

	private Formatter() {
	}

	/**
	 * Returns the name in upper case with underscore to separate words.
	 * 
	 * @param aName a name
	 * @return the name in upper case with underscore to separate words
	 */
	public static String getUpperFromCamelCaseName(String aName) {
		return StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(aName), '_').toUpperCase();
	}

	/**
	 * Returns the name in snake case.
	 * 
	 * @param aName a name
	 * @return the name in snake case
	 */
	public static String getSnakeFromCamelCaseName(String aName) {
		return StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(aName), '_').toLowerCase();
	}
}
