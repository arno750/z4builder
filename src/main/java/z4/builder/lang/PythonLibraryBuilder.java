package z4.builder.lang;

import java.io.IOException;

import z4.builder.AbstractLibraryBuilder;
import z4.builder.BuildException;

/**
 * @author arnaud
 *
 */
public class PythonLibraryBuilder extends AbstractLibraryBuilder {

	@Override
	protected void buildClasses(String targetPath) throws IOException, BuildException {
	}
}
