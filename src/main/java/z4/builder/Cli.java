package z4.builder;

/**
 * Command-line program.
 * 
 * @author Arnaud Wieland
 *
 */
public class Cli {

	/**
	 * Main function of the builder application.
	 * 
	 * @param anArguments the arguments
	 * @throws Exception if an error occurs
	 */
	public static void main(String[] anArguments) throws Exception {
		if ((anArguments.length != 4) && (anArguments.length != 3)) {
			System.out.println("Usage:\n" + "\tjava -jar z4builder.jar <model path> <language> <target path>\n\n"
					+ "<language> is one of java, c++, python or rust\n");
			System.exit(-1);
		}

		String modelPath = anArguments[0];
		String language = anArguments[1];
		String targetPath = anArguments[2];

		AbstractLibraryBuilder builder = LibraryBuilderFactory.get(ProgrammingLanguage.parse(language));
		builder.buildSources(modelPath, targetPath);
		builder.buildResources(modelPath, targetPath);
	}

	/**
	 * Private constructor to avoid instance creation.
	 */
	private Cli() {
	}
}
