package z4.builder;

import z4.builder.lang.CppLibraryBuilder;
import z4.builder.lang.JavaLibraryBuilder;
import z4.builder.lang.PythonLibraryBuilder;
import z4.builder.lang.RustLibraryBuilder;

/**
 * This class if a factory of library builder created depending on the specified
 * programming language (Java, Python, C++...).
 * 
 * @author Arnaud Wieland
 *
 */
public class LibraryBuilderFactory {

	/**
	 * Returns the library builder associated to the specified programming language.
	 * 
	 * @param aProgrammingLanguage a programming language
	 * @return the associated library builder
	 */
	public static AbstractLibraryBuilder get(ProgrammingLanguage aProgrammingLanguage) {
		switch (aProgrammingLanguage) {
			case CPP:
				return new CppLibraryBuilder();
			case JAVA:
				return new JavaLibraryBuilder();
			case PYTHON:
				return new PythonLibraryBuilder();
			case RUST:
				return new RustLibraryBuilder();
			default:
				throw new UnsupportedOperationException(
						String.format("Language %s is not supported", aProgrammingLanguage));
		}
	}

	/**
	 * Private constructor to avoid instance creation.
	 */
	private LibraryBuilderFactory() {
	}
}
