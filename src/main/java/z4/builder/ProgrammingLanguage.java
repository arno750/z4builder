package z4.builder;

/**
 * A family of extensions representing the bundle element within a file
 * container.
 *
 * @author Arnaud Wieland
 */
public enum ProgrammingLanguage {

	JAVA("Java"),
	PYTHON("Python"),
	CPP("C++"),
	RUST("Rust");

	/** file extension. */
	String name;

	/**
	 * Constructs a new object.
	 *
	 * @param aName a name
	 */
	ProgrammingLanguage(final String aName) {
		this.name = aName;
	}

	/**
	 * Returns the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the object matching the name.
	 *
	 * @param aName a name
	 * @return the object matching the specified extension.
	 *
	 */
	public static ProgrammingLanguage parse(final String aName) {
		for (final ProgrammingLanguage language : ProgrammingLanguage.values()) {
			if (language.name.equalsIgnoreCase(aName)) {
				return language;
			}
		}

		throw new IllegalArgumentException(String.format("Unsupported language: %s", aName));
	}
}
