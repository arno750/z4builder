package z4.builder;

/**
 * @author Arnaud Wieland
 *
 */
public class BuildException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6841184946211320999L;
}
