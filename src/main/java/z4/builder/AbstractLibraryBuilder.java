package z4.builder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;

/**
 * This class provides a skeletal implementation of a library builder based on a
 * model.
 * 
 * @author Arnaud Wieland
 *
 */
public abstract class AbstractLibraryBuilder {

	/** Logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractLibraryBuilder.class);

	/**
	 * Builds the library resources based on the model.
	 * 
	 * @param modelPath  the model path
	 * @param targetPath the target path
	 * @throws BuildException if an error occurs while building the library
	 */
	public void buildResources(String modelPath, String targetPath) throws BuildException {
	}

	/**
	 * Builds the library sources based on the model.
	 * 
	 * @param modelPath  the model path
	 * @param targetPath the target path
	 * @throws BuildException if an error occurs while building the library
	 */
	public void buildSources(String modelPath, String targetPath) throws BuildException {
	}

	/**
	 * Updates the model and types classes.
	 * 
	 * @param targetPath the target path
	 * @throws IOException    if a I/O error occurs
	 * @throws BuildException if an error occurs while using the model
	 */
	protected abstract void buildClasses(String targetPath) throws IOException, BuildException;

	/**
	 * Creates a folder (and all the necessary parent folders).
	 * 
	 * @param targetPath       a target path
	 * @param folderPathSuffix a folder path suffix
	 * @throws IOException if an I/O error occurs
	 */
	protected void createFolders(String targetPath, String folderPathSuffix) throws IOException {
		LOGGER.info("Create folder {}{}", targetPath, folderPathSuffix);
		Files.createDirectories(Paths.get(targetPath + folderPathSuffix));
	}

	/**
	 * Copies a class file.
	 * 
	 * @param resourcePath a resource path
	 * @param targetPath   a target path
	 * @throws IOException if an I/O error occurs
	 */
	protected void copyClass(String resourcePath, String targetPath) throws IOException {
		LOGGER.info("Copy class {}", targetPath);
		try (InputStream stream = AbstractLibraryBuilder.class.getResourceAsStream(resourcePath)) {
			Files.copy(stream, Paths.get(targetPath), StandardCopyOption.REPLACE_EXISTING);
		}
	}

	/**
	 * Updates the specified class.
	 * 
	 * @param data         the data used to populate the template
	 * @param resourcePath the source resource path
	 * @param path         the target class path
	 * @throws IOException    if a I/O error occurs
	 * @throws BuildException if an error occurs while using the model
	 */
	protected void buildClass(Map<String, Object> data, String resourcePath, Path path)
			throws IOException, BuildException {
		try (InputStream stream = AbstractLibraryBuilder.class.getResourceAsStream(resourcePath)) {
			Template template = Mustache.compiler().compile(new InputStreamReader(stream));
			Writer writer = Files.newBufferedWriter(path);
			template.execute(data, writer);
			writer.flush();
			writer.close();
		}

		LOGGER.info("Build class {}", path.toString());
	}
}
